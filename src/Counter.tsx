import React, { useCallback, useState } from "react";
import { useCounterContext } from "./CounterContext";

export const Counter: React.FC = () => {
  const [inputValue, setInputValue] = useState(0);

  const { state, actions } = useCounterContext();

  const onChange = useCallback((e: React.ChangeEvent<HTMLInputElement>) => {
    const fixedValue = e.target.value.replace(/[^0-9.]/g, "");
    setInputValue(parseInt(fixedValue));
  }, []);

  return (
    <div className="counter">
      <div className="top-buttons">
        <button onClick={actions.inc}>Inc</button>
        <button onClick={actions.dec}>Dec</button>
      </div>
      <p>{state.count}</p>
      <div className="bottom-buttons">
        <button
          onClick={() => {
            actions.add(inputValue);
            setInputValue(0);
          }}
        >
          Add
        </button>
        <input type="text" value={inputValue} onChange={onChange} />
      </div>
    </div>
  );
};
