import { createContext, useContext } from "react";
import { ICounterProps } from "./_types";

export const CounterContext = createContext({} as ICounterProps);

export const useCounterContext = () => useContext(CounterContext);
