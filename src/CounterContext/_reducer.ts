import { ICounterState, TActions, eActionType } from "./_types";

export const reducer = (
  state: ICounterState,
  action: TActions[eActionType]
): ICounterState => {
  switch (action.type) {
    case eActionType.INC:
      return {
        ...state,
        count: state.count + 1,
      };
    case eActionType.DEC:
      return {
        ...state,
        count: state.count - 1,
      };
    case eActionType.ADD:
      return {
        ...state,
        count: state.count + action.payload,
      };
    default:
      throw new Error(`Wrong action ${action}`);
  }
};
