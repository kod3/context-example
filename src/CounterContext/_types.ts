import React from "react";

export interface ICounterState {
  count: number;
}

export interface ICounterActions {
  inc: () => void;
  dec: () => void;
  add: (n: number) => void;
}

export interface ICounterProps {
  state: ICounterState;
  actions: ICounterActions;
}

export enum eActionType {
  INC = 1,
  DEC = 2,
  ADD = 3,
}

export interface IPayload {
  [eActionType.INC]: undefined;
  [eActionType.DEC]: undefined;
  [eActionType.ADD]: number;
}

export type TActions = {
  [key in eActionType]: IPayload[key] extends undefined
    ? { type: key }
    : { type: key; payload: IPayload[key] };
};

export type TCounterDispatch = React.Dispatch<TActions[eActionType]>;
