import React, { useReducer } from "react";
import { CounterContext } from "./_context";
import { reducer } from "./_reducer";
import { useActions } from "./_use_actions.hook";

export const CounterCountextProvider: React.FC<{
  children: React.ReactNode;
}> = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, { count: 0 });

  const actions = useActions(state, dispatch);

  return (
    <CounterContext.Provider value={{ state, actions }}>
      {children}
    </CounterContext.Provider>
  );
};
