import { useCallback } from "react";
import {
  ICounterActions,
  ICounterState,
  TCounterDispatch,
  eActionType,
} from "./_types";

export const useActions = (
  state: ICounterState,
  dispatch: TCounterDispatch
): ICounterActions => {
  const inc = useCallback(() => {
    dispatch({ type: eActionType.INC });
  }, []);
  const dec = useCallback(() => {
    dispatch({ type: eActionType.DEC });
  }, []);
  const add = useCallback((n: number) => {
    dispatch({ type: eActionType.ADD, payload: n });
  }, []);

  return {
    inc,
    dec,
    add,
  };
};
