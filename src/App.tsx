import { Counter } from "./Counter";

import "./App.scss";
import { CounterCountextProvider } from "./CounterContext";

function App() {
  return (
    <div className="App">
      <CounterCountextProvider>
        <Counter />
      </CounterCountextProvider>
    </div>
  );
}

export default App;
